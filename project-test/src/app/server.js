const express = require("express");
const { Pool, Client } = require("pg");

const app = express();
const port = process.env.PORT || 3000;
const cors = require("cors");
app.use(express.json());

const pool = new Pool({
  user: "postgres",
  host: "localhost",
  database: "postgres",
  password: "fadhlan2422",
  port: 5433,
});

const client = new Client({
  user: "postgres",
  host: "localhost",
  database: "postgres",
  password: "fadhlan2422",
  port: 5433,
});

client.connect();

app.get("/api/barang", (req, res) => {
  pool.query("SELECT * FROM barang", (error, results) => {
    if (error) {
      throw error;
    }
    res.status(200).json(results.rows);
  });
});

app.get("/api/transaksi", (req, res) => {
  pool.query(
    'SELECT Transaksi.*, barang."namaBarang", barang."jenisBarang", barang."stok" FROM transaksi JOIN barang ON transaksi.ID_Barang = barang.no',
    (error, results) => {
      if (error) {
        throw error;
      }
      res.status(200).json(results.rows);
    }
  );
});

app.get("/api/allBarangData", (req, res) => {
  const query = `
    SELECT
        b."no" AS "no",
        b."namaBarang" AS "namaBarang",
        b."jenisBarang" AS "jenisBarang",
        b."stok" AS "stok",
        t."jumlahTerjual" AS "jumlahTerjual",
        t."tanggalTransaksi" AS "tanggalTransaksi"
    FROM
        "barang" AS b
    LEFT JOIN "transaksi" AS t ON b."no" = t."id_barang"
    `;

  pool.query(query, (error, results) => {
    if (error) {
      throw error;
    }
    res.status(200).json(results.rows);
  });
});

app.post("/api/create/barang", (req, res) => {
  const { namaBarang, jenisBarang, stok } = req.body;

  if (!namaBarang || !jenisBarang) {
    return res
      .status(400)
      .json({ error: "Data yang diperlukan tidak tersedia." });
  }

  pool.query(
    'INSERT INTO barang ("namaBarang", "jenisBarang", "stok") VALUES ($1, $2, $3)',
    [namaBarang, jenisBarang, stok],
    (error, results) => {
      if (error) {
        console.error("Kesalahan saat menambahkan data:", error);
        return res
          .status(500)
          .json({ error: "Terjadi kesalahan saat menambahkan data." });
      }
      res.status(201).json({ message: "Data berhasil ditambahkan" });
    }
  );
});

app.get("/api/barang/:id", (req, res) => {
  const no = req.params.id;
  pool.query('SELECT * FROM barang WHERE "no" = $1', [no], (error, results) => {
    if (error) {
      throw error;
    }
    if (error) {
      throw error;
    }
    res.status(200).json(results.rows);
  });
});

app.put("/api/barang/:id", (req, res) => {
  const id = req.params.id;
  const { namaBarang, jenisBarang, stok } = req.body;
  pool.query(
    'UPDATE barang SET "namaBarang" = $1, "jenisBarang" = $2, "stok" = $3 WHERE "no" = $4',
    [namaBarang, jenisBarang, stok, id],
    (error, results) => {
      if (error) {
        throw error;
      }
      res.status(200).json({ message: "Data berhasil diperbarui" });
    }
  );
});

app.post("/api/barang-dengan-transaksi", (req, res) => {
  const jenisBarang = req.body.jenisBarang;

  const query = `
    SELECT b."namaBarang", t."jumlahTerjual", t."tanggalTransaksi"
    FROM barang AS b
    LEFT JOIN transaksi AS t ON b."no" = t.id_barang
    WHERE b."jenisBarang" = $1
    ORDER BY t."jumlahTerjual" ASC;
  `;

  pool.query(query, [jenisBarang], (err, results) => {
    if (err) {
      console.error(err);
      res
        .status(500)
        .json({ error: "Terjadi kesalahan dalam mengambil data." });
    } else {
      res.json(results.rows);
    }
  });
});

app.post("/api/tanggal-transaksi", (req, res) => {
  const { jenisBarang, startDate, endDate } = req.body;

  if (!startDate || !endDate) {
    return res.status(400).json({ error: "Parameter tanggal tidak valid." });
  }

  const query = `
    SELECT b."namaBarang", t."jumlahTerjual", t."tanggalTransaksi"
    FROM barang AS b
    LEFT JOIN transaksi AS t ON b."no" = t.id_barang
    WHERE b."jenisBarang" = $1 AND t."tanggalTransaksi" BETWEEN $2 AND $3
    ORDER BY t."jumlahTerjual" ASC;
  `;

  pool.query(query, [jenisBarang, startDate, endDate], (err, results) => {
    if (err) {
      console.error(err);
      res
        .status(500)
        .json({ error: "Terjadi kesalahan dalam mengambil data." });
    } else {
      res.json(results.rows);
    }
  });
});

app.delete("/api/barang/:id", (req, res) => {
  const no = req.params.id;

  pool.query(
    "DELETE FROM transaksi WHERE id_barang = $1",
    [no],
    (error, results) => {
      if (error) {
        throw error;
      }
     
      pool.query(
        'DELETE FROM barang WHERE "no" = $1',
        [no],
        (error, results) => {
          if (error) {
            throw error;
          }
          res.status(200).json({ message: "Data berhasil dihapus" });
        }
      );
    }
  );
});


app.post("/api/addTransaction", async (req, res) => {
  const { id_barang, namaBarang, jumlahTerjual } = req.body;

  if (!jumlahTerjual) {
    return res
      .status(400)
      .json({ error: "Data yang diperlukan tidak tersedia." });
  }

  const tanggalTransaksi = new Date();

  let query;
  let params;
  let insertQuery;
  let insertParams;

  if (id_barang) {
    query = 'SELECT "stok" FROM barang WHERE "no" = $1';
    params = [id_barang];
  } else if (namaBarang) {
    query = 'SELECT "stok" FROM barang WHERE "namaBarang" = $1';
    params = [namaBarang];
  }

  pool.query(query, params, (error, results) => {
    if (error) {
      console.error("Kesalahan saat mengambil data stok barang:", error);
      return res
        .status(500)
        .json({ error: "Terjadi kesalahan saat menambahkan data." });
    }

    const stokBarang = results.rows[0].stok;

    pool.query(
      'SELECT COALESCE(SUM("jumlahTerjual"), 0) as "totalTerjual" FROM transaksi WHERE "id_barang" = (SELECT "no" FROM barang WHERE "namaBarang" = $1)',
      [namaBarang],
      (error, results) => {
        if (error) {
          console.error("Kesalahan saat mengambil total terjual:", error);
          return res
            .status(500)
            .json({ error: "Terjadi kesalahan saat menambahkan data." });
        }

        const totalTerjual = results.rows[0].totalTerjual;

        if (
          parseInt(totalTerjual) + parseInt(jumlahTerjual) >
          parseInt(stokBarang)
        ) {
          return res.status(400).json({
            error: "Jumlah terjual melebihi sisa stok barang yang tersedia.",
          });
        }

        if (id_barang) {
          insertQuery =
            'INSERT INTO transaksi ("jumlahTerjual", "id_barang", "tanggalTransaksi") VALUES ($1, $2, $3)';
          insertParams = [jumlahTerjual, id_barang, tanggalTransaksi];
        } else if (namaBarang) {
          insertQuery =
            'INSERT INTO transaksi ("jumlahTerjual", "id_barang", "tanggalTransaksi") VALUES ($1, (SELECT "no" FROM barang WHERE "namaBarang" = $2), $3)';
          insertParams = [jumlahTerjual, namaBarang, tanggalTransaksi];
        }

        pool.query(insertQuery, insertParams, (error, results) => {
          if (error) {
            console.error("Kesalahan saat menambahkan data:", error);
            return res
              .status(500)
              .json({ error: "Terjadi kesalahan saat menambahkan data." });
          }
          res.status(201).json({ message: "Data berhasil ditambahkan" });
        });
      }
    );
  });
});



app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
