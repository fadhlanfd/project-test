import { TestBed } from '@angular/core/testing';

import { StokBarangService } from './stok-barang.service';

describe('StokBarangService', () => {
  let service: StokBarangService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StokBarangService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
