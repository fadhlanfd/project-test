import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StokBarangService } from '../../stok-barang.service';
import { Table } from 'primeng/table';
import { MenuItem } from 'primeng/api';
import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { NgZone } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  barangData!: any[];
  barangDataId!: any[];
  barangUpdateDataId!: any[];
  barangDeleteDataId!: any[];
  loading: boolean = true;
  filterData: string | undefined;
  items: MenuItem[] | undefined;
  nomorBerurut: number = 1;
  activeItem: MenuItem | undefined;
  visibleTransaction: boolean = false;
  visibleAppealTransaction: boolean = false;
  visible: boolean = false;
  selectedItem: any;
  selectedTransactionItem: any;
  item: any[] = [];
  viewEditLoadingEdit: boolean = false;
  viewDeleteLoadingEdit: boolean = false;
  viewTextApprove: boolean = true;
  viewEditDialog: boolean = false;
  viewDeleteDialog: boolean = false;
  isFetching: boolean = false;
  isProcessEdit: boolean = false;
  itemDataById: any[] = [];
  transactionAppealId: any[] = [];
  updateIdBarang: any;
  deleteIdBarang: any;
  jenisBarangTypeId: any = {};
  rangeDates: Date[] | undefined;
  groupForm!: FormGroup;

  itemData: dataBarang = {
    namaBarang: '',
    jenisBarang: '',
    stok: '',
    id: '',
  };

  itemTransaksi: dataTransaksi = {
    jenisBarang: '',
  };
  appealDataTransaction: any;
  barangDataIdEdit: any;
  jenisBarangType: any;
  billerDisplayNameSelected: any = [];

  constructor(
    private barangService: StokBarangService,
    private ngZone: NgZone,
    private formBuilder: FormBuilder,
    private datepipe: DatePipe,
    private http: HttpClient,
    private router: Router
  ) {}

  ngOnInit() {
    this.barangService.getAllBarangList().subscribe((data: any) => {
      this.barangData = data;
      if (this.barangData) {
        this.barangData = this.barangData.map((barang, index) => ({
          ...barang,
          nomor: this.nomorBerurut + index,
        }));
      }

      this.loading = false;
    });

    this.billerDisplayNameSelected = [];

    this.barangService.getBarangList().subscribe((data: any) => {
        var arrItemBarang = data.map(function (obj: { no: any; namaBarang: any; }) {
          return { no: obj.no, namaBarang: obj.namaBarang }
        });
        for (let i = 0; i < arrItemBarang.length; i++) {
          this.item.push({
            label: arrItemBarang[i]['namaBarang'],
            value: arrItemBarang[i]['no'],
          });
        }

      const uniqueJenisBarangSet = new Set(
        data.map((item: { jenisBarang: any }) => item.jenisBarang)
      );
      const uniqueJenisBarangArray = Array.from(uniqueJenisBarangSet);

      this.jenisBarangType = uniqueJenisBarangArray.map((jenisBarang: any) => {
        return {
          label: jenisBarang,
          value: jenisBarang,
        };
      });
    });

    this.groupForm = this.formBuilder.group({
      rangeDates: [new Date(), Validators.required],
    });


    this.items = [{ label: 'Dashboard', icon: 'pi pi-fw pi-home' }];

    this.activeItem = this.items[0];
  }

  get f() {
    return this.groupForm.controls;
  }

  handleDateRangeSelect() {
    let startDate = this.datepipe.transform(
      this.groupForm.controls['rangeDates'].value[0],
      'yyyy-MM-dd'
    );
    let endDate = this.datepipe.transform(
      this.groupForm.controls['rangeDates'].value[1],
      'yyyy-MM-dd'
    );

    if (this.rangeDates != undefined) {
      startDate;
      endDate;
    }

    let payload: any = {};
    payload = {
      jenisBarang: this.itemTransaksi.jenisBarang,
      startDate: startDate,
      endDate: endDate,
    };

    this.barangService.rangeDateTransaction(payload).subscribe(
      (resp: any) => {
        this.appealDataTransaction = resp;

        if (this.appealDataTransaction) {
          this.appealDataTransaction = this.appealDataTransaction.map(
            (barang: any, index: number) => ({
              ...barang,
              nomor: this.nomorBerurut + index,
            })
          );
        }
      },
      (error) => {
        console.error('Gagal membandingkan transaksi:', error);

        Swal.fire({
          icon: 'error',
          title: 'Gagal Membandingkan Transaksi',
          text: 'Transaksi Gagal Dibandingkan!',
        });
        this.visibleTransaction = false;
      }
    );
  }

  appealTransactionData(jenisBarang: string) {
    if (!jenisBarang) {
      Swal.fire({
        icon: 'error',
        title: 'Gagal Melakukan Perbandingan Transaksi',
        text: 'Harap isi semua data sebelum melakukan perbandingan transaksi!',
      });
      this.visibleAppealTransaction = false;
      return;
    }

    this.visibleAppealTransaction = true;

    const data = {
      jenisBarang,
    };

    this.barangService.appealTransaction(data).subscribe(
      (resp: any) => {
        this.appealDataTransaction = resp;

        if (this.appealDataTransaction) {
          this.appealDataTransaction = this.appealDataTransaction.map(
            (barang: any, index: number) => ({
              ...barang,
              nomor: this.nomorBerurut + index,
            })
          );
        }
        this.itemTransaksi.jenisBarang = jenisBarang;
      },
      (error) => {
        console.error('Gagal membandingkan transaksi:', error);

        Swal.fire({
          icon: 'error',
          title: 'Gagal Membandingkan Transaksi',
          text: 'Transaksi Gagal Dibandingkan!',
        });
        this.visibleTransaction = false;
      }
    );
  }

  addTransaction(namaBarang: string, jumlahTerjual: string) {
    if (!namaBarang || !jumlahTerjual) {
      Swal.fire({
        icon: 'error',
        title: 'Gagal Menambah Transaksi',
        text: 'Harap isi semua data sebelum menambah transaksi!',
      });
      this.visibleTransaction = false;
      return;
    }

    const tanggalTransaksi = new Date();

    const data = {
      namaBarang,
      jumlahTerjual,
      tanggalTransaksi: tanggalTransaksi.toISOString(),
    };

    this.barangService.addTransaction(data).subscribe(
      (response) => {

        Swal.fire({
          icon: 'success',
          title: 'Sukses Menambah Transaksi',
          text: 'Transaksi Telah Berhasil Ditambahkan!',
        }).then(() => {
          window.location.reload();
        });
        this.visibleTransaction = false;

        this.barangService.getAllBarangList().subscribe((data: any) => {
          this.barangData = data;
        });

        this.ngZone.runOutsideAngular(() => {
          setTimeout(() => {
            this.ngZone.run(() => {
              this.barangData = [...this.barangData];
            });
          }, 0);
        });
      },
      (error) => {
        console.error('Gagal menambahkan transaksi:', error.error.error);
        if (error.status == 400) {
          Swal.fire({
            icon: 'error',
            title: 'Gagal Menambah Transaksi',
            text: error.error.error,
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Gagal Menambah Transaksi',
            text: 'Transaksi Gagal Ditambahkan di database.',
          });
        }
        this.visibleTransaction = false;
      }
    );
  }

  viewDelete(data: any) {
    this.viewDeleteDialog = true;
    const payload = data;
    this.deleteIdBarang = payload;
    this.barangService.getBarang(payload).subscribe((resp: any) => {
      this.barangDataId = resp;
      let tempResultById = [];
      for (let i = 0; i < resp.length; i++) {
        let tempData = resp[i];
        tempResultById.push(tempData);
      }

      this.itemDataById = tempResultById;
      this.itemData.namaBarang = this.itemDataById[0].namaBarang;
      this.itemData.jenisBarang = this.itemDataById[0].jenisBarang;
      this.itemData.stok = this.itemDataById[0].stok;
      this.isFetching = false;
      this.viewDeleteLoadingEdit = false;
      this.viewTextApprove = true;
    });
  }

  onSubmitDeleteData() {
    this.viewDeleteLoadingEdit = true;
    this.viewTextApprove = false;

    this.barangService.deleteBarang(this.deleteIdBarang).subscribe(
      (resp: any) => {
        this.barangDeleteDataId = resp;
        Swal.fire({
          icon: 'success',
          title: 'Sukses Menghapus Barang',
          text: 'Barang Telah Berhasil Dihapus!',
        }).then(() => {
          window.location.reload();
        });
        this.viewDeleteDialog = false;
        this.barangService.getAllBarangList().subscribe((data: any) => {
          this.barangData = data;
        });
        this.ngZone.runOutsideAngular(() => {
          setTimeout(() => {
            this.ngZone.run(() => {
              this.barangData = [...this.barangData];
            });
          }, 0);
        });
      },
      (error) => {
        console.error('Gagal mengedit data:', error);
        Swal.fire({
          icon: 'error',
          title: 'Gagal Menghapus Barang',
          text: 'Gagal Menghapus Data di database.',
        });
        this.viewDeleteDialog = false;
      }
    );
  }

  viewEdit(data: any) {
    this.viewEditDialog = true;
    const payload = data;
    this.updateIdBarang = payload;
    this.barangService.getBarang(payload).subscribe((resp: any) => {
      this.barangDataIdEdit = resp;
      let tempResultById = [];
      for (let i = 0; i < resp.length; i++) {
        let tempData = resp[i];
        tempResultById.push(tempData);
      }

      this.itemDataById = tempResultById;
      this.itemData.namaBarang = this.itemDataById[0].namaBarang;
      this.itemData.jenisBarang = this.itemDataById[0].jenisBarang;
      this.itemData.stok = this.itemDataById[0].stok;
      this.isFetching = false;
      this.viewEditLoadingEdit = false;
      this.viewTextApprove = true;
    });
  }

  onSubmitEditData() {
    this.viewEditLoadingEdit = true;
    this.viewTextApprove = false;

    this.barangService
      .updateBarang(this.updateIdBarang, this.itemData)
      .subscribe(
        (resp: any) => {
          this.barangUpdateDataId = resp;
          Swal.fire({
            icon: 'success',
            title: 'Sukses Mengedit Barang',
            text: 'Barang Telah Berhasil Diedit!',
          }).then(() => {
            window.location.reload();
          });
          this.viewEditDialog = false;
          this.barangService.getAllBarangList().subscribe((data: any) => {
            this.barangData = data;
          });
          this.ngZone.runOutsideAngular(() => {
            setTimeout(() => {
              this.ngZone.run(() => {
                this.barangData = [...this.barangData];
              });
            }, 0);
          });
        },
        (error) => {
          console.error('Gagal mengedit data:', error);
          Swal.fire({
            icon: 'error',
            title: 'Gagal Mengedit Barang',
            text: 'Gagal Mengedit Data di database.',
          });
          this.viewEditDialog = false;
        }
      );
  }

  createNewBarang(namaItem: string, jenisItem: string, stok: string) {
    if (!namaItem || !jenisItem) {
      Swal.fire({
        icon: 'error',
        title: 'Gagal Menambah Barang',
        text: 'Harap isi semua data sebelum menambah barang!',
      });
      this.visible = false;
      return;
    }

    const barangExists = this.barangData.some(
      (barang) => barang.namaBarang === namaItem
    );

    if (barangExists) {
      Swal.fire({
        icon: 'error',
        title: 'Gagal Menambah Barang',
        text: 'Nama Barang sudah ada dalam database!',
      });
      this.visible = false;
      return;
    }

    const data = { namaBarang: namaItem, jenisBarang: jenisItem, stok };

    this.barangService.createBarang(data).subscribe(
      (response) => {
        Swal.fire({
          icon: 'success',
          title: 'Sukses Menambah Barang',
          text: 'Barang Telah Berhasil Ditambahkan!',
        }).then(() => {
          window.location.reload();
        });

        this.visible = false;

        this.barangService.getAllBarangList().subscribe((data: any) => {
          this.barangData = data;
        });

        this.ngZone.runOutsideAngular(() => {
          setTimeout(() => {
            this.ngZone.run(() => {
              this.barangData = [...this.barangData];
            });
          }, 0);
        });
      },
      (error) => {
        console.error('Gagal menambahkan data:', error);
        Swal.fire({
          icon: 'error',
          title: 'Gagal Menambah Barang',
          text: 'Barang Gagal Ditambahkan di database.',
        });
        this.visible = false;
      }
    );
  }

  clear(table: Table) {
    this.filterData = '';
    table.clear();
  }

  clearTableModal(table: Table) {
    table.clear();
    table.reset();
  }

  showDialog() {
    this.visible = true;
  }

  showDialogTransaction() {
    this.visibleTransaction = true;
  }

  showDialogAppealTransaction() {
    this.visibleAppealTransaction = true;
  }
}

interface dataBarang {
  namaBarang: String;
  jenisBarang: String;
  stok: String;
  id: String;
}

interface dataTransaksi {
  jenisBarang: String;
}
