import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class StokBarangService {
  constructor(private http: HttpClient) { }

  getAllBarangList() {
    return this.http.get('/api/allBarangData');
  }

  getBarangList() {
    return this.http.get('/api/barang');
  }

  createBarang(data: any) {
    return this.http.post('/api/create/barang', data);
  }

  getBarang(id: any) {
    return this.http.get(`/api/barang/${id}`);
  }

  updateBarang(id: any, data: any) {
    return this.http.put(`/api/barang/${id}`, data);
  }

  deleteBarang(id: number) {
    return this.http.delete(`/api/barang/${id}`);
  }

  addTransaction(data: any) {
    return this.http.post('/api/addTransaction', data)
  }

  appealTransaction(data: any) {
    return this.http.post('/api/barang-dengan-transaksi', data)
  }

  rangeDateTransaction(data: any) {
    return this.http.post('/api/tanggal-transaksi', data);
  }
}
